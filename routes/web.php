<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//admin panel
Route::resource('/buses' , 'BusController');

Route::get('/', 'HomeController@index');

Route::post('/tickets', 'HomeController@ticket');

Route::post('/cancel', 'HomeController@cancel');

