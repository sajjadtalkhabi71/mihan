@extends('master')

@section('content')

    @php $oldDate = NULL; @endphp

    @foreach($buses as $bus)

            @if($oldDate != $bus->move_date)
                <h4 class="text-info my-3">سرویس های موجود روز {{ $bus->move_date }}</h4>
            @endif

            <div class="card w-100 mb-2">
            @php $reserve = 0; @endphp
            @for ($i = 0 ; $i < count($bus->tickets); $i++)
                @php
                    if ($bus->tickets[$i]['status'] == 1){
                    $reserve += $bus->tickets[$i]['seat_number'];
                   }
                @endphp
            @endfor

            <div class="card-body d-flex">
                <span class="card-link"><span>مبدا: </span> {{ $bus->origin }}</span>
                <span class="card-link"><span>مقصد: </span>{{ $bus->goal }}</span>
                <span class="card-link"><span>تاریخ حرکت: </span>{{ $bus->move_date }}</span>
                <span class="card-link"><span>ساعت حرکت: </span>{{ $bus->move_time }}</span>
                <span class="card-link"><span>تعداد صندلی خالی: </span>{{ $bus->seat - $reserve }}</span>
                <button type="button" class="btn btn-success mr-auto" data-toggle="modal" data-target="#exampleModal" data-whatever="{{ $bus->id }}"> رزرو بلیط </button>
            </div>
        </div>
        @php
            if ($oldDate != $bus->move_date)
                $oldDate = $bus->move_date
        @endphp
    @endforeach
@endsection






