@extends('master')

@section('content')

    <div class="main">
        <div class="page-header head-section">

            <div class="d-flex my-3">
                <h2>اتوبوس ها</h2>
                <a href="{{ route('buses.create') }}" class="btn btn-primary mr-auto">افزودن اتوبوس</a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>نوع اتوبوس</th>
                    <th>مبدا</th>
                    <th>مقصد</th>
                    <th>تاریخ حرکت</th>
                    <th>ساعت حرکت</th>
                    <th>ظرفیت</th>
                    <th>تنظیمات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($buses as $bus)
                    <tr>
                        <td>{{ $bus->type }}</td>
                        <td>{{ $bus->origin }}</td>
                        <td>{{ $bus->goal }}</td>
                        <td>{{ $bus->move_date }}</td>
                        <td>{{ $bus->move_time }}</td>
                        <td>{{ $bus->seat }}</td>
                        <td>
                            <form action="{{ route('buses.destroy'  , $bus->id) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs" dir="ltr">
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                    <a href="{{ route('buses.edit' , $bus->id) }}" class="btn btn-primary">ویرایش</a>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div style="text-align: center">
            {!! $buses->render() !!}
        </div>
    </div>

@endsection


