@extends('master')

@section('content')

    <div class="main">
        <div class="page-header head-section mt-3">
            <h2>مشخصات سفر</h2>
        </div>
        <form class="form-horizontal" action="{{ route('buses.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('Admin.errors')

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="type" class="control-label">نوع اتوبوس</label>
                            <select name="type" id="type" class="form-control">
                                <option selected>VIP</option>
                                <option >اسکانیا</option>
                                <option >معمولی</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="origin" class="control-label">مبدا</label>
                            <input type="text" class="form-control" name="origin" id="origin" placeholder="مبدا را وارد کنید" value="{{ old('origin') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="goal" class="control-label">مقصد</label>
                            <input type="text" class="form-control" name="goal" id="goal" placeholder="مقصد را وارد کنید" value="{{ old('goal') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="move_date" class="control-label">تاریخ حرکت</label>
                            <input type="date" class="form-control" name="move_date" id="move_date" placeholder="تاریخ را وارد کنید" value="{{ old('move_date') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="move_time" class="control-label">ساعت حرکت</label>
                            <input type="time" class="form-control" name="move_time" id="move_time" placeholder="ساعت حرکت را وارد کنید" value="{{ old('move_time') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="seat" class="control-label">ظرفیت(نفر) </label>
                            <input type="number" class="form-control" name="seat" id="seat" placeholder="ظرفیت اتوبوس را وارد کنید" value="{{ old('seat') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="cost" class="control-label">مبلغ</label>
                            <input type="number" class="form-control" name="cost" id="cost" placeholder="مبلغ را وارد کنید" value="{{ old('cost') }}">
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-danger">ایجاد</button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>


@endsection




