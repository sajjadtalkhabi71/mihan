@extends('master')

@section('content')

    <div class="text-center my-5">
        <h2>{{  $message }}</h2>
    </div>

    <div class="text-center">
        <a href="/" class="btn badge-info">بازگشت</a>
    </div>

@endsection
