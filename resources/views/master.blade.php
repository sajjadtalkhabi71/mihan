<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href="{{  url('/') }}/css/bootstrap.min.css" type="text/css" rel="stylesheet">

        <style>
            body {text-align: right;}  a {cursor: pointer;}
        </style>
    </head>

    <body>
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="/">میهن نور فراهان</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/">خانه </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="modal" data-target="#staticBackdrop">کنسل کردن بلیط</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/buses">اتوبوس ها</a>
                        </li>
                    </ul>

                </div>
            </nav>
        </header>


        <!-- main body --->
        <div class="container mt-4">

           @yield('content')

        </div>
        <!-- /-- main body --->



        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">لطفا شماره بلیط مورد نظر را وارد نمایید</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" action="/cancel" enctype="multipart/form-data" method="post">
                            {{ csrf_field() }}
                            @include('Admin.errors')
                            <div class="form-group">
                                <label for="ticket_number" class="col-form-label">شماره بلیط:</label>
                                <input type="tel" name="ticket_number" class="form-control" id="ticket_number">
                            </div>
                            <div class="form-group">
                                <label for="phone_number" class="col-form-label">شماره موبایل:</label>
                                <input type="tel" name="phone_number" class="form-control" id="phone_number">
                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                            <button type="submit" class="btn btn-danger">حذف</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">اطلاعات خود را وارد کنید</h5>
                        <button type="button" class="close mr-auto ml-0" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" action="/tickets" enctype="multipart/form-data" method="post">
                            {{ csrf_field() }}
                            @include('Admin.errors')
                            <div class="form-group">
                                <label for="bus-number" class="col-form-label">شماره اتوبوس:</label>
                                <input readonly type="number" name="bus_id" class="form-control" id="bus-number">
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-form-label">نام:</label>
                                <input type="text" name="name" class="form-control" id="name">
                            </div>
                            <div class="form-group">
                                <label for="family_name" class="col-form-label">نام خانوادگی:</label>
                                <input type="text" name="family_name" class="form-control" id="family_name">
                            </div>
                            <div class="form-group">
                                <label for="phone_number" class="col-form-label">شماره موبایل:</label>
                                <input type="tel" name="phone_number" class="form-control" id="phone_number">
                            </div>
                            <div class="form-group">
                                <label for="seat_number" class="col-form-label">تعداد صندلی:</label>
                                <input type="number" name="seat_number" class="form-control" id="seat_number">
                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                            <button type="submit" class="btn btn-primary">ارسال</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /-- Modal -->


        <!-- /-- Scripts -->
        <script src="{{  url('/') }}/js/jquery-3.5.1.min.js" type="text/javascript"></script>
        <script src="{{  url('/') }}/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{  url('/') }}/js/script.js" type="text/javascript"></script>
    </body>
</html>
