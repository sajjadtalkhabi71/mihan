@extends('master')

@section('content')

    <!-- main body --->

        <h2 class="my-3">رزو بلیط شما با موفقیت انجام شد</h2>
        <a href="/" class="mb-3">خروج</a>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>شماره بلیط</th>
                    <th>نام</th>
                    <th>نام خانوادگی</th>
                    <th>شماره تماس</th>
                    <th>تعداد صندلی</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $code['ticket_number'] }}</td>
                    <td>{{ $request->name }}</td>
                    <td>{{ $request->family_name }}</td>
                    <td>{{ $request->phone_number }}</td>
                    <td>{{ $request->seat_number }}</td>
                </tr>
                </tbody>
                <thead>
                <tr>
                    <th>نوع اتوبوس</th>
                    <th>مبدا</th>
                    <th>مقصد</th>
                    <th>تاریخ حرکت</th>
                    <th>ساعت حرکت</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $bus->type }}</td>
                    <td>{{ $bus->origin }}</td>
                    <td>{{ $bus->goal }}</td>
                    <td>{{ $bus->move_date }}</td>
                    <td>{{ $bus->move_time }}</td>
                </tr>
                </tbody>
            </table>
        </div>


@endsection
