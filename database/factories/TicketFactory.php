<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Ticket;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Ticket::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'family_name' => $faker->name,
        'phone_number' => $faker->numberBetween(10000000000,100000000000),
        'ticket_number' => $faker->numberBetween(1000,1000000),
        'seat_number' => $faker->numberBetween(1,5),
    ];
});
