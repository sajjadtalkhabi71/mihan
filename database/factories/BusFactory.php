<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Bus;
use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(Bus::class, function (Faker $faker) {
    return [
        'origin' => $faker->city,
        'goal' => $faker->city,
        'cost' => $faker->numberBetween(100,100000),
        'type' => $faker->randomElement(['VIP' , 'اسکانیا' , 'معمولی']),
        'move_date' => $faker->dateTimeBetween('now' , '+7 day'),
        'move_time' => $faker->time(),
        'seat' => $faker->randomElement(['44' , '33']),
    ];
});
