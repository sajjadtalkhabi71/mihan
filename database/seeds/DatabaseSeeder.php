<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Bus::class , 20)->create()->each(function ($tic){
            $tic->tickets()->saveMany(factory(App\Ticket::class,2)->make());
        });

    }
}
