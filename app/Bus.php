<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{

    protected $fillable =[
        'type' , 'origin' , 'goal' , 'seat' ,'move_date' , 'move_time' , 'cost',
    ];

    public function tickets()
    {
        return $this->hasMany(Ticket::class, 'bus_id');
    }

}
