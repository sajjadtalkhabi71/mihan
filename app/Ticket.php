<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'bus_id','name' , 'family_name' , 'phone_number' , 'seat_number' ,'ticket_number'
    ];


    public function buses()
    {
        return $this->belongsTo(Bus::class);
    }
}
