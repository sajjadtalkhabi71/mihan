<?php

namespace App\Http\Controllers;


use App\Bus;
use App\Http\Requests\BusRequest;


class BusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buses = Bus::latest()->paginate(10);
        return view('Admin.all' , compact('buses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BusRequest $request)
    {
        Bus::create($request->all());

        return redirect(route('buses.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bus  $bus
     * @return \Illuminate\Http\Response
     */
    public function show(Bus $bus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bus  $bus
     * @return \Illuminate\Http\Response
     */
    public function edit(Bus $bus)
    {
        return view('Admin.edit' , compact('bus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BusRequest $request
     * @param \App\Bus $bus
     * @return \Illuminate\Http\Response
     */
    public function update(BusRequest $request, Bus $bus)
    {
        $bus->update($request->all());

        return redirect(route('buses.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bus  $bus
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bus $bus)
    {
        $bus->delete();
        return redirect(route('buses.index'));
    }
}
