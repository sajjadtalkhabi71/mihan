<?php

namespace App\Http\Controllers;



use App\Bus;
use App\Http\Requests\CancelRequest;
use App\Http\Requests\TicketRequest;
use App\Ticket;



class HomeController extends Controller
{
    public function index()
    {
        $buses = Bus::orderBy('move_date')->get();
        return view('index' , compact('buses'));
    }

    public function ticket(TicketRequest $request)
    {
        //create ticket
        $bus = Bus::find($request->bus_id);

        $reserve = 0;
        for ($i = 0 ; $i < count($bus->tickets); $i++){

            if ($bus->tickets[$i]['status'] == 1){
                $reserve += $bus->tickets[$i]['seat_number'];
            }
        }

        if ($bus->seat - $reserve >= $request->seat_number)
        {
            $substr = substr($request->phone_number, 4, 5 );
            $code =rand(1000,10000);
            $substr.=$code;
            $code = ['ticket_number' => $substr];
            Ticket::create(array_merge($request->all() , $code));

        }else{
            $message = 'متاسفانه تعداد بلیط درخواستی شما بیش از ظرفیت اتوبوس می باشد، لطفا اتوبوس دیگری را انتخاب نمایید';
            return view('alert' , compact('message'));
        }


        return view('ticket' , compact('bus' , 'request' , 'code'));
    }

    public function cancel(CancelRequest $request)
    {
        $ticket = Ticket::whereTicket_number($request->ticket_number)->wherePhone_number($request->phone_number)->first();
        if (is_null($ticket)){
            $message = 'بلیطی با این مشخصات یافت نشد';
            return view('alert' , compact('message'));

        }else{
            if ($ticket->status == 1){
                $ticket->status = 0;
                $ticket->save();

                $message = 'بلیط شما با موفیت حذف شد';
                return view('alert' , compact('message'));
            }else{
                $message = 'بلیط با این مشخصات قبلا کنسل شده است.';
                return view('alert' , compact('message'));
            }

        }

    }

}
